package com.fstn.t4html;

import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stephen on 25/03/2016.
 * Utils for block
 */
public class BlockFacade
{
    private static Logger logger = LoggerFactory.getLogger(BlockFacade.class.getName());

    /**
     * Get block from file content
     *
     * @param fileName file name
     * @param fileContent file content
     * @return List<Block>
     */
    public static List<Block> fileContentToBlock(String fileName,
                                                 String fileContent) {
        Pattern blockPattern = Pattern.compile(Config.START_FLAG + ":" + "([^:]*):([^:]*)-->");
        Matcher m = blockPattern.matcher(fileContent);
        List<Block> blocks = new ArrayList<>();
        while (m.find()) {
            int startIndex = m.start();
            String verb = m.group(1).trim();
            String name = m.group(2).trim();
            String startTag = Config.START_FLAG + ":" + verb + ":" + name + "-->";
            String endTag = Config.END_FLAG + ":" + verb + ":" + name + "-->";
            logger.info("Looking for betweenTags: " + startTag + " " + endTag);
            //remove String that are before current match in order to retreat another block already done
            String splitResult = fileContent.substring(startIndex);
            String[] contentAsArray = StringUtils.substringsBetween(splitResult, startTag, endTag);
            if (contentAsArray != null && contentAsArray.length > 0) {
                String content = contentAsArray[0].trim();
                Block block = new Block(name, verb, content, fileName);
                logger.debug("Find block " + block);
                if (!blocks.contains(block)) {
                    blocks.add(block);
                }
            }
        }
        return blocks;
    }
}

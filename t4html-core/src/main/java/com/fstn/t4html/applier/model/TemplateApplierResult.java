package com.fstn.t4html.applier.model;

import com.fstn.t4html.model.Block;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephen on 24/03/2016.
 * Result that return which block has been applied
 */
public class TemplateApplierResult
{
    private List<Block> appliedBlock = new ArrayList<>();
    private String appliedPackage;

    public List<Block> getAppliedBlock() {
        return appliedBlock;
    }

    public void setAppliedBlock(List<Block> appliedBlock) {
        this.appliedBlock = appliedBlock;
    }

    public String getAppliedPackage() {
        return appliedPackage;
    }

    public void setAppliedPackage(String appliedPackage) {
        this.appliedPackage = appliedPackage;
    }

    @Override
    public String toString() {
        return "TemplateApplierResult{" +
            "appliedBlock=" + appliedBlock +
            ", appliedPackage='" + appliedPackage + '\'' +
            '}';
    }
}

package com.fstn.t4html.builder;

import com.fstn.t4html.applier.TemplateApplier;
import com.fstn.t4html.applier.model.TemplateApplierResult;
import com.fstn.t4html.builder.model.DistBuilderResult;
import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;
import com.fstn.t4html.reader.FileWalker;
import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import org.apache.commons.io.FileUtils;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.io.FileWriter;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by stephen on 21/03/2016.
 */
public class DistBuilder
{
    private static Logger logger = LoggerFactory.getLogger(DistBuilder.class.getName());
    private Path pathFrom;

    public static DistBuilder read() {
        return new DistBuilder();
    }

    public DistBuilder from(String pathFromAsString) {
        this.pathFrom = Paths.get(pathFromAsString);
        return this;
    }


    public DistBuilderResult build() throws IOException {
        DistBuilderResult distBuilderResult = new DistBuilderResult();
        File baseFolderFile = pathFrom.toFile();
        List<String> availablePackages = Arrays.asList(baseFolderFile.list());
        List<Block> blocks = new ArrayList<>();
        Comparator<String> blockPriorityComparator = (aName, bName) -> aName.compareTo(bName);
        availablePackages
            .stream()
            .sorted(blockPriorityComparator.reversed())
            .forEachOrdered(packageFolder -> {
                logger.info("Running Template on " + packageFolder);
                try {
                    logger.info("Apply block template on " + packageFolder);
                    TemplateApplierResult templateApplierResult = applyBlock(
                        pathFrom + File.separator + packageFolder, blocks);
                    templateApplierResult.setAppliedPackage(packageFolder);
                    distBuilderResult.getApplyBlocks().add(templateApplierResult);
                    logger.info("get block template on " + packageFolder);
                    List<Block> newCustomBlock = getNewBlock(pathFrom + File.separator + packageFolder);
                    blocks.addAll(newCustomBlock);
                    // keep order for replace purpose
                    Collections.reverse(blocks);
                } catch (IOException e) {
                    logger.error(
                        "Unable to build package " + pathFrom + File.separator + packageFolder.toString(),
                        e);
                }
            });

        distBuilderResult.setApplyPackages(availablePackages);
        distBuilderResult.setPathFrom(baseFolderFile.toString());
        distBuilderResult.setPathTo(baseFolderFile.toString() + File.separator + "dist");
        return distBuilderResult;
    }

    /**
     * Apply templating on specific patch
     *
     * @param packageFolder
     * @param blocks
     */
    private TemplateApplierResult applyBlock(String packageFolder, List<Block> blocks) throws IOException {
        TemplateApplierResult templateApplierResult = TemplateApplier
            .read()
            .from(packageFolder)
            .to(packageFolder + "/dist/")
            .fileEndsWith(".html")
            .apply(blocks);

        java.nio.file.Path dst = Paths.get(packageFolder + "/dist/");
        dst.toFile().setWritable(true);
        return templateApplierResult;
    }

    private List<Block> getNewBlock(String packageFolder) throws IOException {
        List<Block> blocks = BlockParser.read()
                                        .fileEndsWith(Config.BLOCK_EXTENSION)
                                        .from(packageFolder + "/dist")
                                        .parse();
        return blocks;
    }

//    public void otherMethod() throws IOException {
//        PathMatcher cssMatcher = FileSystems.getDefault().getPathMatcher("glob:**/*.css");
//        PathMatcher jsMatcher = FileSystems.getDefault().getPathMatcher("glob:**/*.js");
//        PathMatcher jsMinMatcher = FileSystems.getDefault().getPathMatcher("glob:**/*.min.js");
//
//        FileUtils.copyDirectory(new File(pathFrom.toString()), new File(pathTo.toString()));
//        FileWalker.read().from(pathTo.toString()).visitor(new SimpleFileVisitor<Path>()
//        {
//            @Override
//            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
//                if (jsMatcher.matches(path) && !jsMinMatcher.matches(path)) {
//                    logger.info("Building path " + path);
//                    String pathContent = Files.lines(path).collect(Collectors.joining());
//                    if (!pathContent.isEmpty()) {
//                        Reader reader = new StringReader(pathContent);
//                        JavaScriptCompressor compressor = new JavaScriptCompressor(reader, new ErrorReporter()
//                        {
//                            @Override
//                            public void warning(String s, String s1, int i, String s2, int i1) {
//                                //TODO handle error
//                                logger.error(s + ": " + s1 + ": " + s2 + ":" + i + ":" + i1);
//                            }
//
//                            @Override
//                            public void error(String s, String s1, int i, String s2, int i1) {
//                                //TODO handle error
//                                logger.error(s + ": " + s1 + ": " + s2 + ":" + i + ":" + i1);
//
//                            }
//
//                            @Override
//                            public EvaluatorException runtimeError(String s, String s1, int i, String s2, int i1) {
//                                //TODO handle error
//                                logger.error(s + ": " + s1 + ": " + s2 + ":" + i + ":" + i1);
//                                return new EvaluatorException(s);
//                            }
//                        });
//                        FileWriter fileWriter = new FileWriter(path.toString());
//                        compressor.compress(fileWriter, Integer.MAX_VALUE, true, false, false, true);
//                        fileWriter.flush();
//                    }
//                } else if (cssMatcher.matches(path)) {
//                    logger.info("Building path " + path);
//                    String pathContent = Files.lines(path).collect(Collectors.joining());
//                    if (!pathContent.isEmpty()) {
//                        Reader reader = null;
//                        try {
//                            reader = new StringReader(Files.lines(path).collect(Collectors.joining()));
//                            CssCompressor compressor = new CssCompressor(reader);
//                            FileWriter fileWriter = new FileWriter(path.toString());
//                            compressor.compress(fileWriter, Integer.MAX_VALUE);
//                            fileWriter.flush();
//                        } catch (IOException e) {
//                            logger.error("Unable to build Dist " + e);
//                        } finally {
//                            if (reader != null) {
//                                reader.close();
//                            }
//                        }
//                    }
//                }
//                return FileVisitResult.CONTINUE;
//            }
//        }).execute();
//    }
}

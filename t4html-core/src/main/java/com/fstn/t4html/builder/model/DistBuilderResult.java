package com.fstn.t4html.builder.model;

import com.fstn.t4html.applier.model.TemplateApplierResult;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephen on 24/03/2016.
 * Build Result, allow to get information  that help to debug build
 */
@XmlRootElement
public class DistBuilderResult
{
    private String pathTo;
    private String pathFrom;
    private List<String> applyPackages = new ArrayList<>();
    private List<TemplateApplierResult> applyBlocks = new ArrayList<>();

    public String getPathTo() {
        return pathTo;
    }

    public void setPathTo(String pathTo) {
        this.pathTo = pathTo;
    }

    public String getPathFrom() {
        return pathFrom;
    }

    public void setPathFrom(String pathFrom) {
        this.pathFrom = pathFrom;
    }

    public List<String> getApplyPackages() {
        return applyPackages;
    }

    public void setApplyPackages(List<String> applyPackages) {
        this.applyPackages = applyPackages;
    }


    public List<TemplateApplierResult> getApplyBlocks() {
        return applyBlocks;
    }

    public void setApplyBlocks(List<TemplateApplierResult> applyBlocks) {
        this.applyBlocks = applyBlocks;
    }

    @Override
    public String toString() {
        return "DistBuilderResult{" +
            "pathTo='" + pathTo + '\'' +
            ", pathFrom='" + pathFrom + '\'' +
            ", applyPackages=" + applyPackages +
            ", applyBlocks=" + applyBlocks +
            '}';
    }
}

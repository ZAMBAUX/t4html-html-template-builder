package com.fstn.t4html.model;

import com.fstn.t4html.config.Config;

import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by stephen on 11/03/2016.
 */
public class Block
{

    private String name;
    private String verb;
    private String content;
    private String origin;

    public Block() {
        super();
    }

    public Block(String name, String position, String content, String origin) {
        this.name = name;
        this.verb = position;
        this.content = content;
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @XmlTransient
    public String getStartTag() {
        return Config.START_FLAG + ":" + verb + ":" + name + "-->";
    }

    @XmlTransient
    public String getEndTag() {
        return Config.END_FLAG + ":" + verb + ":" + name + "-->";
    }

    @XmlTransient
    public String getDescribeStartTag() {
        return Config.START_FLAG + ":" + Config.DESCRIBE_VERB + ":" + name + "-->";
    }

    @XmlTransient
    public String getDescribeEndTag() {
        return Config.END_FLAG + ":" + Config.DESCRIBE_VERB + ":" + name + "-->";
    }

    @XmlTransient
    public String getContentWithDescribeTags() {
        return getDescribeStartTag() + getContent() + getDescribeEndTag();
    }

    @XmlTransient
    public String getContentWithTags() {
        return getStartTag() + getContent() + getEndTag();
    }

    @Override
    public String toString() {
        return "Block{" +
            " name='" + name + '\'' +
            ", verb='" + verb + '\'' +
            ", content='" + content + '\'' +
            '}';
    }
}

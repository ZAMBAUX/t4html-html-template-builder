package com.fstn.t4html.parser;

import com.fstn.t4html.BlockFacade;
import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.reader.FileWalker;
import com.fstn.t4html.reader.visitor.FileContentVisitor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stephen on 11/03/2016.
 */
public class BlockParser
{

    private String pathFrom;
    private Logger logger = LoggerFactory.getLogger(BlockParser.class.getName());
    private String extension;

    public static BlockParser read() {
        return new BlockParser();
    }

    /**
     * Path that contains block
     *
     * @param pathFrom
     * @return
     */
    public BlockParser from(String pathFrom) {
        this.pathFrom = pathFrom;
        return this;
    }

    /**
     * Path that contains block
     *
     * @param extension
     * @return
     */
    public BlockParser fileEndsWith(String extension) {
        this.extension = extension;
        return this;
    }

    /**
     * Parse Directory to extract block from blocks files
     *
     * @return
     * @throws IOException
     */
    public List<Block> parse() throws IOException {
        List<Block> blocks = new ArrayList<>();

        logger.debug("Replace file with extension: " + extension);

        Map<String, String> allBlocksString = new HashMap<>();
        FileWalker
            .read()
            .from(pathFrom)
            .visitor(new FileContentVisitor("glob:**/*" + extension + "", allBlocksString))
            .execute();

        allBlocksString.forEach((fileName, fileContent) -> {
            blocks.addAll(BlockFacade.fileContentToBlock(fileName, fileContent));
        });
        logger.debug("Blocks Result: " + blocks);
        return blocks;
    }


}

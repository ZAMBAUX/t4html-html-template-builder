package com.fstn.t4html.writer;

import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by stephen on 20/03/2016.
 */
public class BlockWriter
{

    private static Logger logger = LoggerFactory.getLogger(BlockWriter.class.getName());
    private Block block;
    private Path pathTo;

    public static BlockWriter write() {
        return new BlockWriter();
    }

    public BlockWriter block(Block block) {
        this.block = block;
        return this;
    }

    public BlockWriter to(String to) throws IOException {
        Path pathTo = Paths.get(to);
        if (!Files.exists(pathTo)) {
            Files.createFile(pathTo);
        }
        this.pathTo = pathTo;
        return this;
    }

    public void execute() throws IOException {
        StringBuffer blockAsString = new StringBuffer();
        blockAsString.append(block.getContentWithTags());
        long sameBlockCount = BlockParser.read()
                                         .from(pathTo.toString())
                                         .fileEndsWith(Config.BLOCK_EXTENSION)
                                         .parse()
                                         .stream()
                                         .filter(tmpBlock -> block.getStartTag().equals(tmpBlock.getStartTag()))
                                         .count();
        if (sameBlockCount > 0) {
            FileWriter.replaceBetween(block.getStartTag(), block.getEndTag(), blockAsString, pathTo);
        } else {
            FileWriter.append(blockAsString, pathTo);
        }

    }
}

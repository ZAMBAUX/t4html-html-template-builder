package com.fstn.t4html.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

/**
 * Created by SZA on 14/03/2016.
 * Utils that provide easy way to write inside file
 */
public class FileWriter
{
    private static Logger logger = LoggerFactory.getLogger(FileWriter.class.getName());

    /**
     * Write into path file
     *
     * @param fileContent
     * @param filePath
     * @throws IOException
     */
    public static void write(StringBuffer fileContent, Path filePath) throws IOException {
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filePath.toFile());
            os.write(fileContent.toString().getBytes());
        } catch (IOException e) {
            logger.error("Unable to write in " + filePath.toString(), e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    logger.info("Unable to close outputStream", e);
                }
            }
        }
    }

    /**
     * Append into path file
     *
     * @param fileContent
     * @param filePath
     * @throws IOException
     */
    public synchronized static void append(StringBuffer fileContent, Path filePath) {
        FileOutputStream os = null;
        try {
            StringBuffer toWrite = new StringBuffer(Files.lines(filePath).collect(Collectors.joining("\n")));
            logger.info("FileWriter: before Append File Content " + toWrite.toString());
            toWrite.append("\n" + fileContent);
            os = new FileOutputStream(filePath.toFile());
            os.write(toWrite.toString().getBytes());
            os.close();
            logger.info("FileWriter: after Append " + toWrite);
        } catch (IOException e) {
            logger.error("Unable to append in " + filePath.toString(), e);

        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    logger.info("Unable to close outputStream", e);
                }
            }
        }
    }

    /**
     * @param startTag
     * @param endTag
     * @param blockAsString
     * @param filePath
     */
    public synchronized static void replaceBetween(String startTag, String endTag, StringBuffer blockAsString,
                                                   Path filePath) {
        FileOutputStream os = null;
        try {

            //TODO revoir pour la gestion de plusieurs fois le meme tag dans le meme fichier
            StringBuffer toWrite = new StringBuffer(Files.lines(filePath).collect(Collectors.joining("\n")));
            logger.info(
                "FileWriter: before Replace File Content " + toWrite.toString() + " between " + startTag + " and " + endTag);
            int start = toWrite.indexOf(startTag);
            int end = toWrite.indexOf(endTag);
            if (start != -1 && end != -1) {
                end += endTag.length();
                toWrite = toWrite.replace(start, end, blockAsString.toString());
                os = new FileOutputStream(filePath.toFile());
                os.write(toWrite.toString().getBytes());
                logger.info("FileWriter: after Replace " + toWrite.toString() + " from " + start + " to " + end);
            } else {
                logger.error(
                    "Unable to replace between " + startTag + " and " + endTag + " for " + filePath.toString());
            }
        } catch (IOException e) {
            logger.error("Unable to replace between " + startTag + " and " + endTag + " for " + filePath.toString(), e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    logger.info("Unable to close outputStream", e);
                }
            }
        }
    }
}

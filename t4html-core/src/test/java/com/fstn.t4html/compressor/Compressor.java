package com.fstn.t4html.compressor;

import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import com.yahoo.platform.yui.compressor.YUICompressor;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * Created by stephen on 21/03/2016.
 */
public class Compressor {

    @Test
    public void testJavascriptCompressor() throws IOException {
        // prepare input reader

        String javascript = "MyModule\n" +
                "    .directive('itLoader',['$http','$rootScope', function ($http,$rootScope) {\n" +
                "        return {\n" +
                "            restrict : 'EA',\n" +
                "            scope:true,\n" +
                "            template : '<span class=\"fa-stack\">' +\n" +
                "                            '<i class=\"fa fa-refresh fa-stack-1x\" ng-class=\"{\\'fa-spin\\':$isLoading}\">' +\n" +
                "                            '</i>' +\n" +
                "                        '</span>',\n" +
                "            link : function ($scope) {\n" +
                "                $scope.$watch(function() {\n" +
                "                    if($http.pendingRequests.length>0){\n" +
                "                        $scope.$applyAsync(function(){\n" +
                "                            $scope.$isLoading = true;\n" +
                "                        });\n" +
                "\n" +
                "                    } else {\n" +
                "                        $scope.$applyAsync(function(){\n" +
                "                            $scope.$isLoading = false;\n" +
                "                        });\n" +
                "\n" +
                "                    }\n" +
                "                });\n" +
                "\n" +
                "            }\n" +
                "        }\n" +
                "    }]\n" +
                ");";
        Reader reader = new StringReader(javascript);
        JavaScriptCompressor compressor = new JavaScriptCompressor(reader, null);

        // write compressed output
        Writer writer = new StringWriter();
        compressor.compress(writer, 100, true, false, false, false);
        writer.flush();
        Assert.assertEquals("javascript compression error","MyModule.directive(\"itLoader\",[\"$http\",\"$rootScope\",function(b,a){return{restrict:\"EA\",scope:true,template:'<span class=\"fa-stack\"><i class=\"fa fa-refresh fa-stack-1x\" ng-class=\"{\\'fa-spin\\':$isLoading}\"></i></span>',link:function(c){c.$watch(function(){if(b.pendingRequests.length>0){c.$applyAsync(function(){c.$isLoading=true\n" +
                "})}else{c.$applyAsync(function(){c.$isLoading=false})}})}}}]);",writer.toString());
    }


    @Test
    public void testCSSCompressor() throws IOException {
        // prepare input reader

        String javascript = ".it-autocomplete-content {\n" +
                "  top: 0px;\n" +
                "  right: 0;\n" +
                "  bottom: 0;\n" +
                "  left: 0;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                ".it-autocomplete-select {\n" +
                "  background-color: white;\n" +
                "  cursor: pointer;\n" +
                "  padding-left: 10px;\n" +
                "  padding-right: 10px;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                ".it-autocomplete-select:hover {\n" +
                "  background-color: whitesmoke;\n" +
                "}\n" +
                "\n" +
                ".it-autocomplete-selected:hover {\n" +
                "  background-color: #337ab6;\n" +
                "}\n" +
                "\n" +
                ".it-autocomplete-selected {\n" +
                "  background-color: #337ab7;\n" +
                "  cursor: default;\n" +
                "  color: white;\n" +
                "}\n" +
                "\n" +
                ".ui-grid-filter-container .it-autocomplete-container {\n" +
                "  position: fixed;\n" +
                "  z-index: 9999;\n" +
                "}";
        Reader reader = new StringReader(javascript);
        CssCompressor compressor = new CssCompressor(reader);

        // write compressed output
        Writer writer = new StringWriter();
        compressor.compress(writer,Integer.MAX_VALUE);
        writer.flush();
        Assert.assertEquals(".it-autocomplete-content{top:0;right:0;bottom:0;left:0;width:100%}.it-autocomplete-select{background-color:white;cursor:pointer;padding-left:10px;padding-right:10px;width:100%}.it-autocomplete-select:hover{background-color:whitesmoke}.it-autocomplete-selected:hover{background-color:#337ab6}.it-autocomplete-selected{background-color:#337ab7;cursor:default;color:white}.ui-grid-filter-container .it-autocomplete-container{position:fixed;z-index:9999}",writer.toString());
    }
}

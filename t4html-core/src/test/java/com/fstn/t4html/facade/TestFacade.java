package com.fstn.t4html.facade;

import com.fstn.t4html.BlockFacade;
import com.fstn.t4html.model.Block;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

/**
 * Created by stephen on 25/03/2016.
 */
public class TestFacade
{

    public static Block getBaseBlock(String fromFolder) throws IOException {
        Block baseBlock;
        String baseBlockContent = Files.lines(Paths.get(fromFolder+ File.separator+"0-Base"+File.separator
                                                            +"view.html"))
                                       .collect(Collectors.joining("\n"));
        baseBlock = BlockFacade.fileContentToBlock(fromFolder, baseBlockContent).get(0);
        return baseBlock;
    }

    public static Block getFrBlock(String fromFolder) throws IOException {

        String frBlockContent = Files.lines(Paths.get(fromFolder+File.separator+"2-FR"+File.separator
                                                          +"fr.html.blocks"))
                                     .collect(Collectors.joining("\n"));
        Block frBlock = BlockFacade.fileContentToBlock(fromFolder, frBlockContent).get(0);
        return frBlock;
    }

    public static Block getEuBlock(String fromFolder) throws IOException {
        String euBlockContent = Files.lines(Paths.get(fromFolder+File.separator+"1-EU"+File.separator
                                                          +"eu.html.blocks"))
                                     .collect(Collectors.joining("\n"));
        Block frBlock = BlockFacade.fileContentToBlock(fromFolder, euBlockContent).get(0);
        return frBlock;
    }
}

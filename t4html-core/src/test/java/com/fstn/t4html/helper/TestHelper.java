package com.fstn.t4html.helper;

import com.fstn.t4html.builder.DistBuilder;
import com.fstn.t4html.builder.model.DistBuilderResult;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

/**
 * Created by stephen on 25/03/2016.
 */
public class TestHelper
{
    private String fromPath;
    private String expectedBlocksResult;
    private int count;

    public static TestHelper run() {
        return  new TestHelper();
    }

    public TestHelper from(String fromPath) {
        this.fromPath= fromPath;
        return this;
    }

    public TestHelper assertResult(String expectedBlocksResult) {
        this.expectedBlocksResult = expectedBlocksResult;
        return this;
    }

    public void runTemplate() {
        try {
            DistBuilderResult distBuilderResult = DistBuilder.read().from(fromPath).build();

            // getting nb blocks applied
            Assert.assertEquals("After build result is not good",count,
                                distBuilderResult.getApplyBlocks()
                                                 .stream()
                                                 .mapToInt(templateResult -> templateResult.getAppliedBlock().size())
                                                 .reduce(Integer::sum).getAsInt());

            String finalViewContent = Files.lines(Paths.get(fromPath+ File.separator+"0-Base"+File.separator
                                                                +"dist"+File.separator+"view.html")).collect(
                Collectors.joining("\n"));

            Assert.assertEquals("apply with block",expectedBlocksResult, finalViewContent);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage() + ":" + new File(".").getAbsolutePath());
        }
    }

    public TestHelper assertCount(int count) {
        this.count = count;
        return this;
    }
}

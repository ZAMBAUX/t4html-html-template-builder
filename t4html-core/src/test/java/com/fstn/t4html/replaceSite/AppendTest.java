package com.fstn.t4html.replaceSite;

import com.fstn.t4html.builder.DistBuilder;
import com.fstn.t4html.builder.model.DistBuilderResult;
import com.fstn.t4html.config.Config;
import com.fstn.t4html.facade.TestFacade;
import com.fstn.t4html.helper.TestHelper;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;
import com.fstn.t4html.applier.TemplateApplier;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by stephen on 17/03/2016.
 */
public class AppendTest {
    String fromFolder = "src"+File.separator+"test"+File.separator+"resources"+File.separator+ "appendSite";
    String startFlag = "<!--start-block:describe:footerline-total-net-amount-->\n";
    String endFlag = "\n<!--end-block:describe:footerline-total-net-amount-->";

    @Test
    public void simpleModuleCase() throws IOException {
        Block baseBlock = TestFacade.getBaseBlock(fromFolder);
        Block frBlock = TestFacade.getFrBlock(fromFolder);
        Block euBlock = TestFacade.getEuBlock(fromFolder);

        String expectedBlocksResult =
            startFlag+
                baseBlock.getContent()+
                endFlag+
                frBlock.getContent()+
                euBlock.getContent();
        ;
        TestHelper.run().from(fromFolder).assertResult(expectedBlocksResult).runTemplate();

    }
}

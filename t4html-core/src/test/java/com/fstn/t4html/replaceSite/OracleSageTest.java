package com.fstn.t4html.replaceSite;

import com.fstn.t4html.applier.TemplateApplier;
import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by stephen on 25/03/2016.
 */
public class OracleSageTest
{
    String fromFolder = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "oracleSageTest" + File.separator;
    String toFolder = "src" + File.separator + "test-result" + File.separator + "resources" + File.separator + "oracleSageTest" + File.separator;

    @Test
    public void simpleModuleCase() {

        String expectedBlocksResult = "";
        try {

            FileUtils.deleteDirectory(new File(toFolder));

            List<Block> blocks = BlockParser.read().fileEndsWith(Config.BLOCK_EXTENSION).from(fromFolder).parse();
            //Assert.assertEquals("incorrect block parsing", expectedBlocksResult, blocks.toString());
            TemplateApplier
                .read()
                .to(toFolder)
                .from(fromFolder)
                .fileEndsWith(".html")
                .apply(blocks);

            String finalViewContent = Files.lines(Paths.get(toFolder + File.separator + "html" + File.separator
                                                                + "0-Base" + File.separator+"src"+File.separator
                                                                + "app" + File.separator + "features" +File.separator+"login"+File.separator
                                                                +"loginView.html"))
                                           .collect(Collectors.joining("\n"));

            Assert.assertEquals("apply with block", expectedBlocksResult, finalViewContent);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage() + ":" + new File(".").getAbsolutePath());
        }
    }
}
package com.fstn.t4html.replaceSite;

import com.fstn.t4html.BlockFacade;
import com.fstn.t4html.builder.DistBuilder;
import com.fstn.t4html.builder.model.DistBuilderResult;
import com.fstn.t4html.config.Config;
import com.fstn.t4html.helper.TestHelper;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;
import com.fstn.t4html.applier.TemplateApplier;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by stephen on 14/03/2016.
 */
public class ReplaceTest {

    String fromFolder = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "replaceSite";
    String startFlag = "<!--start-block:describe:footerline-total-net-amount-->";
    String endFlag = "<!--end-block:describe:footerline-total-net-amount-->";

    @Test
    public void simpleModuleCase() throws IOException {

        String frBlockContent = Files.lines(Paths.get(fromFolder+File.separator+"2-FR"+File.separator
                                                            +"0.fr.html.blocks"))
                                     .collect(Collectors.joining("\n"));
        Block frBlock = BlockFacade.fileContentToBlock(fromFolder,frBlockContent).get(0);
        String expectedBlocksResult =
            startFlag+
            frBlock.getContent()+"\n"+
            endFlag;
        ;
        TestHelper.run().from(fromFolder).assertResult(expectedBlocksResult).runTemplate();

    }
}

package com.fstn.t4html.service;

import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;
import com.fstn.t4html.writer.BlockWriter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by stephen on 18/03/2016.
 * Expose block in restapi service
 */
@Path("/blocks")
public class BlocksService
{

    @GET()
    @Path("/available")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Block> findAvailableBlocks() throws IOException {
        List<Block> blockResult = BlockParser
            .read()
            .fileEndsWith(Config.BLOCK_EXTENSION)
            .from(FolderConfig.baseFolder + "/src/")
            .parse();
        return blockResult;
    }

    @POST()
    @Path("/available")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public synchronized void addAvailable(Block block) throws IOException {
        String savePath = FolderConfig.baseFolder + File.separator + "10.ps";
        if (!Files.exists(Paths.get(savePath))) {
            Files.createDirectory(Paths.get(savePath));
        }

        if (!block.getContent().equals("")) {
            BlockWriter.write()
                       .block(block)
                       .to(savePath + File.separator + "ps.html.blocks")
                       .execute();
        }
    }

    /**
     * Return custom block that extends block with blockName
     *
     * @param blockName
     * @return
     * @throws IOException
     */
    @GET()
    @Path("/available/{blockName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Block> findAvailableBlocksForBlockName(@PathParam("blockName") String blockName) throws IOException {
        List<Block> blockResult = BlockParser
            .read()
            .fileEndsWith(Config.BLOCK_EXTENSION)
            .from(FolderConfig.baseFolder)
            .parse()
            .stream()
            .filter(block -> block.getName().equals(blockName))
            .collect(Collectors.toList());
        return blockResult;
    }

    @GET()
    @Path("/original")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Block> findOriginalBlocks() throws IOException {
        List<Block> blockResult = BlockParser
            .read()
            .fileEndsWith(".html")
            .from(FolderConfig.baseFolder)
            .parse();
        return blockResult;
    }

    @GET()
    @Path("/original/{blockName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Block findAvailableBlocksByName(@PathParam("blockName") String blockName) throws IOException {
        Optional<Block> blockResult = BlockParser
            .read()
            .fileEndsWith(".html")
            .from(FolderConfig.baseFolder)
            .parse()
            .stream()
            .filter(block -> block.getName()
                                  .equals(blockName)).findFirst();

        return blockResult.get();
    }
}

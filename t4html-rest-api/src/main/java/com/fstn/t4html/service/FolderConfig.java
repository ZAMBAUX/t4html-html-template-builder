package com.fstn.t4html.service;

import java.io.File;

/**
 * Created by stephen on 22/03/2016.
 */
public class FolderConfig
{
    public static String baseFolder = "../t4html-core" + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "oracleSage/";
    public static String distFolder = "../t4html-core" + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "oracleSage/dist/";

}

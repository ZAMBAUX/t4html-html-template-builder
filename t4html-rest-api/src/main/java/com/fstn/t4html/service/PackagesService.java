package com.fstn.t4html.service;

import com.fstn.t4html.applier.TemplateApplier;
import com.fstn.t4html.applier.model.TemplateApplierResult;
import com.fstn.t4html.builder.DistBuilder;
import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.builder.model.DistBuilderResult;
import com.fstn.t4html.parser.BlockParser;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by stephen on 21/03/2016.
 */
@Path("/packages")
public class PackagesService
{
    private static Logger logger = LoggerFactory.getLogger(PackagesService.class.getName());

    @GET()
    @Path("/compress")
    @Produces(MediaType.APPLICATION_JSON)
    public void compressPackage() throws IOException {
        DistBuilder
            .read()
            .from(FolderConfig.baseFolder)
            .to(FolderConfig.distFolder)
            .build();
    }

    @GET()
    @Path("/build")
    @Produces(MediaType.APPLICATION_JSON)
    public synchronized DistBuilderResult buildPackage() throws Exception {
        if (FolderConfig.baseFolder == null) {
            throw new Exception("Unable to package because config pathFrom is missing ");
        }
        if (FolderConfig.distFolder == null) {
            throw new Exception("Unable to package because config distFolder is missing ");
        }

        String pathTo = FolderConfig.distFolder;
        String pathFrom = FolderConfig.baseFolder;
        DistBuilderResult distBuilderResult = DistBuilder.read().from(pathFrom).to(pathTo).build();
        return distBuilderResult;
    }



}

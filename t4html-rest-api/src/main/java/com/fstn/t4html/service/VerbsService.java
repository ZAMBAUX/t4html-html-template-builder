package com.fstn.t4html.service;

import com.fstn.t4html.config.Config;
import com.fstn.t4html.model.Block;
import com.fstn.t4html.parser.BlockParser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephen on 18/03/2016.
 * Expose verb in restapi service
 */
@Path("/verbs")
public class VerbsService
{

    @GET()
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> findAvailableBlocks() throws IOException {
        List<String> verbsResult = new ArrayList<>();
        verbsResult.add(Config.AFTER_VERB);
        verbsResult.add(Config.BEFORE_VERB);
        verbsResult.add(Config.APPEND_VERB);
        verbsResult.add(Config.PREPEND_VERB);
        verbsResult.add(Config.AROUND_VERB);
        verbsResult.add(Config.REPLACE_VERB);
        return verbsResult;
    }

}
